<%-- 
    Document   : TranslatedDNA
    Created on : Jan 10, 2017, 6:54:58 PM
    Author     : Leon
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="css/style.css" type="text/css"/>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Translated DNA Sequence</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script src="javascript/showAndHideSequences.js" type="text/javascript"></script>
    </head>
    <body>
        <nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li><a href="index.jsp">DNA Translator</a></li>
        <li  class="active"><a>Amino Acid Sequences</a></li>
      </ul>
    </div>
  </div>
</nav>

        <h1 class="putInCenter">Results</h1>
        <div class="breakWords">
        <h3>The designated forward DNA strands to be translated:</h3>
        <br/>
        <p><strong>First forward reading frame:</strong></p>
        <div id="firstForward">
            <p>
                 ${sessionScope.OriginalWorkSeq}
            </p>
        </div>
        <button onclick="myFunction1()">Show/Hide</button>
        <br/> <br/>
        <p><strong>Second forward reading frame:</strong></p>
        <div id="secondForward">
            <p>
                ${sessionScope.OriginalWorkSeq2}
            </p>
        </div>
        <button onclick="myFunction2()">Show/Hide</button>
        <br/> <br/>
        <p><strong>Third forward reading frame:</strong></p>
        <div id="thirdForward">
            <p>
                ${sessionScope.OriginalWorkSeq3}
            </p>
        </div>
        <button onclick="myFunction3()">Show/Hide</button>
        <h3>It's reverse strand, in the -5, -3 direction:</h3>
        <br/>
        <p><strong>First reverse reading frame:</strong></p>
        <div id="firstReverse">
            <p>
                 ${sessionScope.OriginalComplementWorkSeq}
            </p>
        </div>
        <button onclick="myFunction4()">Show/Hide</button>
        <br/> <br/>
        <p><strong>Second reverse reading frame:</strong></p>
        <div id="secondReverse">
            <p>
                ${sessionScope.OriginalComplementWorkSeq2}
            </p>
        </div>
        <button onclick="myFunction5()">Show/Hide</button>
        <br/> <br/>
        <p><strong>Third reverse reading frame:</strong></p>
        <div id="thirdReverse">
            <p>
                ${sessionScope.OriginalComplementWorkSeq3}
            </p>
        </div>
        <button onclick="myFunction6()">Show/Hide</button>
        <h3 class="putInCenter">These are the six-frame translation results:</h3>
        </div>
        <hr/>
        <h2 class="putInCenter">Forward</h2>
        <hr/>
        <p>
            <strong>First reading frame: </strong><br/>
            ${sessionScope.OriginalAminoAcids1}
            <br/> <br/>
            <strong>Second reading frame: </strong><br/>
            ${sessionScope.OriginalAminoAcids2}
            <br/> <br/>
            <strong>Third reading frame: </strong><br/>
            ${sessionScope.OriginalAminoAcids3}
        </p>
        <hr/>
        <h2 class="putInCenter">Reverse</h2>
        <hr/>
        <p>
            <strong>First reading frame: </strong><br/>
            ${sessionScope.ComplementAminoAcids1}
            <br/> <br/>
            <strong>Second reading frame: </strong><br/>
            ${sessionScope.ComplementAminoAcids2}
            <br/> <br/>
            <strong>Third reading frame: </strong><br/>
            ${sessionScope.ComplementAminoAcids3}
        </p>
        <hr/>
        <footer class="container-fluid text-center">
            <jsp:include page="include/Footer.jsp" />
        </footer>
    </body>
</html>
