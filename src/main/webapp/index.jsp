<%-- 
    Document   : index
    Created on : Jan 21, 2017, 7:38:38 PM
    Author     : Leon
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
    <head>
        <link rel="stylesheet" href="css/style.css" type="text/css"/>

        <title>DNA Translator web page</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
          <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script src="javascript/showSeqLength.js" type="text/javascript"></script>
</head>
    </head>
    <body>
        <nav class="navbar navbar-inverse">
            <div class="container-fluid">
              <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>                        
                </button>
              </div>
              <div class="collapse navbar-collapse" id="myNavbar">
                <ul class="nav navbar-nav">
                  <li class="active"><a href="#">DNA Translator</a></li>
                  <li><a href="TranslatedDNA.jsp">Amino Acid Sequences</a></li>
                </ul>
              </div>
            </div>
      </nav>
        <div class="container-fluid text-center">  
        <h1> DNA sequence translator</h1>
        <h4>
            This website translates any valid DNA string to its complementary amino acids. Use this <br/>
            website to skip the transcription process and go immediately to translation.
            <br/> <br/>
            How to use: Insert a valid nucleotide code into the first input box. <br/>
            Be aware: any lowercase nucleotides put in will be converted to uppercase letters. <br/>
            In the 2 smaller boxes, put in the start position and stop position of the nucleotide sequence that should be translated. <br/>
            Leaving one of the boxes empty will cause the program to start from the <br/>
            first nucleotide / use every nucleotide until the last one, depending on the empty box.
            <br/> <br/>
        </h4>
        <form method="POST" action="Translate.do" id="dnaForm">
            DNA Sequence: <br/>
            <!--<input type="text" class="inputbox" name="DNA_Seq"/> -->
            <textarea class="inputbox" name="DNA_Seq" id="DNA_Seq" form="dnaForm"></textarea> 
            <br/><div id="counter"></div> <br/>
            Translation start position. Note: Count starts from 1!: <br/>
            <input type="number" name="start_pos" value="1" /> <br/> <br/>
            Translation stop position: <br/>
            <input type="number" name="stop_pos" id="stop_pos" class="stop_pos"/> <br/> <br/>
            Amino Acid representation: <br/>
            <select id="mySelect" name="mySelect">
                <option value="one">1-letter code</option>
                <option value="three">3-letter code</option>
                <option value="whole">Entire names</option>
            </select>
            <br/> <br/>
            <input type="submit" value="Submit"/>
        </form>
        <c:choose>
            <c:when test="${requestScope.Error_number == 1}">
                <h1>An illegal character was found in the nucleotide input or the input was empty</h1> 
            </c:when> 
            <c:when test ="${requestScope.Error_number == 2}">
                <h1>An illogical input in the start/stop position was detected</h1>
            </c:when>
        </c:choose>
        </div>
        <br/> <br/>
        <footer class="container-fluid text-center">
            <jsp:include page="include/Footer.jsp" />
        </footer>
    </body>
</html>