/* 
 * Copyright (c) 2016 Leon Drenth [leon-drenth@hotmail.com].
 * All rights reserved
 * Version 0.0.1
 */

$( document ).ready(function() {
    console.log( "ready!" );
});

$(function(){
    $("#firstForward").hide();
    $("#secondForward").hide();
    $("#thirdForward").hide();
    $("#firstReverse").hide();
    $("#secondReverse").hide();
    $("#thirdReverse").hide();
});

function myFunction1() {
    var x = document.getElementById('firstForward');
    if (x.style.display === 'none') {
        x.style.display = 'block';
    } else {
        x.style.display = 'none';
    }
}

function myFunction2() {
    var x = document.getElementById('secondForward');
    if (x.style.display === 'none') {
        x.style.display = 'block';
    } else {
        x.style.display = 'none';
    }
}

function myFunction3() {
    var x = document.getElementById('thirdForward');
    if (x.style.display === 'none') {
        x.style.display = 'block';
    } else {
        x.style.display = 'none';
    }
}

function myFunction4() {
    var x = document.getElementById('firstReverse');
    if (x.style.display === 'none') {
        x.style.display = 'block';
    } else {
        x.style.display = 'none';
    }
}

function myFunction5() {
    var x = document.getElementById('secondReverse');
    if (x.style.display === 'none') {
        x.style.display = 'block';
    } else {
        x.style.display = 'none';
    }
}

function myFunction6() {
    var x = document.getElementById('thirdReverse');
    if (x.style.display === 'none') {
        x.style.display = 'block';
    } else {
        x.style.display = 'none';
    }
}


