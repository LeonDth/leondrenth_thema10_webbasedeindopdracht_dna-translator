/* 
 * Copyright (c) 2016 Leon Drenth [leon-drenth@hotmail.com].
 * All rights reserved
 * Version 0.0.1
 */

$( document ).ready(function() {
    console.log( "ready!" );
    $("#DNA_Seq").on('keyup paste', function(){
    var Characters = $("#DNA_Seq").val().replace(/ /g,"").replace(/(\r\n|\n|\r)/g,"").length;
    $("#counter").text("Amount of nucleotides:" + (Characters));
    $(".stop_pos").val(Characters);
    });
});
