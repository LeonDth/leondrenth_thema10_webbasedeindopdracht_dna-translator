/*
 * Copyright (c) 2016 Leon Drenth [leon-drenth@hotmail.com].
 * All rights reserved
 * Version 0.0.1
 */

package nl.bioinf.leondrenth.dna_translator.model;

/**
 * This class is responsible for checking the input sequence for
 * any characters that aren't a nucleotide.
 * @author Leon Drenth
 * @version 0.0.1
 */
public class dnaChecker {
    /**
     * String holding the input DNA sequence.
     */
    private final String dnaSequence;
    
    /**
     * Boolean that will tell the servlet if there is an error present in the
     * input sequence. Default is False: No error found.
     */
    private boolean error = false;
    
    /**
     * When a new instance of the class is created, it will immediately check
     * for faulty input. This method also assigns the input string to
     * the private variable of this class.
     * @param dnaSeq The input DNA sequence
     */
    public dnaChecker(final String dnaSeq) {
        this.dnaSequence = dnaSeq;
        checkDNA();
    }
    
    /**
     * This method searches for any chars that do not match A, C, G or T. Changes the 
     * error boolean to true if it finds anything.
     */
    private void checkDNA() {
        if (dnaSequence.matches(".*[^ATCG].*") || "".equals(dnaSequence)) {
            error = true;
        }
    }
    
    /**
     * This method will be called on by the servlet to see if an error should be
     * shown or if it can continue the translation with the current sequence. 
     * @return a boolean for the servlet.
     */
    public final boolean errorPresence() {
        return error;
    }
}
