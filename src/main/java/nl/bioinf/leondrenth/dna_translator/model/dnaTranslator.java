/*
 * Copyright (c) 2016 Leon Drenth [leon-drenth@hotmail.com].
 * All rights reserved
 * Version 0.0.1
 */

package nl.bioinf.leondrenth.dna_translator.model;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * This class is mainly responsible for 2 things:
 * Creating the complementary DNA strand of the input sequence and
 * assigning amino acids to the 3 reading frames of the original strand and the
 * complementary strand.
 * @author Leon Drenth
 * @version 0.0.1
 */
public class dnaTranslator {
    /**
     * String that will contain the user input sequence.
     */
    private final String dna_Sequence;
    
    /**
     * HashMap that will contain the nucleotides to amino acid dictionary.
     */
    private final HashMap<String, String> dna_AminoDict = new HashMap<>();
    
    /**
     * The custom start position of the DNA sequence. 
     */
    private final int startPos;
    
    /**
     * The custom stop position of the DNA sequence.
     */
    private final int stopPos;
    
    /**
     * The string that will remember the original input sequence with the possible custom start/stop positions.
     */
    private String originalWorkSeq;
    
    /**
     * The string that will remember the original input sequence, but with 1 frame shift in comparison to the original.
     */
    private String originalWorkSeq2;
    
    /**
     * The string that will remember the original input sequence, but with 2 frame shifts in comparison to the original.
     */
    private String originalWorkSeq3;
    
    /**
     * The complementary DNA sequence of the original DNA sequence with possible custom start/stop positions.
     */
    private String originalComplementWorkSeq;
    
    /**
     * The complementary DNA sequence of the original DNA sequence, but with 1 frame shift in comparison to the original complement strand.
     */
    private String originalComplementWorkSeq2;
    
    /**
     * The complementary DNA sequence of the original DNA sequence, but with 2 frame shifts in comparison to the original complement strand.
     */
    private String originalComplementWorkSeq3;
    
    /**
     * This string will also contain the input sequence with custom start/stop positions, but this string
     * will shrink 1 nucleotide for every reading frame.
     */
    private String workSequence;
    
    /**
     * This string will also contain the complementary input sequence with custom start/stop positions, 
     * but this string will shrink 1 nucleotide for every reading frame.
     */
    private String complementDNA;
    
    /**
     * String that will hold the input DNA sequence divided in triplets.
     */
    private String splitUpSeq; 
    
    /**
     * String that will hold the complementary input DNA sequence divided in triplets.
     */
    private String splitUpComp;
    
    /**
     * String with amino acids for forward reading frame 1.
     */
    private String originalAminoSeq;
    
    /**
     * String with amino acids for forward reading frame 2.
     */
    private String originalAminoSeq1;
    
    /**
     * String with amino acids for forward reading frame 3.
     */
    private String originalAminoSeq2;
    
    /**
     * Complementary string with amino acids for reverse reading frame 1.
     */
    private String complementAminoSeq;
    
    /**
     * Complementary string with amino acids for reverse reading frame 2.
     */
    private String complementAminoSeq1;
    
    /**
     * Complementary string with amino acids for reverse reading frame 3.
     */
    private String complementAminoSeq2;

    /**
     * Integer used for assigning another DNA reading frame to another string variable.
     */
    private int alternateFrameCounter;
    
    /**
     * Method that is called on when a new instance of the class is called upon.
     * Assigns the input values to local class variables.
     * @param dnaSeq The DNA sequence put in by the user.
     * @param startPos custom DNA start position.
     * @param stopPos custom DNA stop position.
     */
    public dnaTranslator(final String dnaSeq, final int startPos, final int stopPos) {
        this.alternateFrameCounter = 1;
        this.dna_Sequence = dnaSeq;
        this.stopPos = stopPos + 1;
        this.startPos = startPos;
    }
    
    /**
     * Method responsible for starting up all the other methods that will create the translations. 
     * @param aminoRepresentation The option selected by the user that determines how the amino acids will be shown
     *                            on the result page.
     */
    public final void initializer(final String aminoRepresentation) {
        createDNA_AminoDict(aminoRepresentation);
        makeWorkSequence();
        makeComplement();
        splitdnaSequence();
        assignAminos();
        for (int i = 1; i <= 2; i++) {
            alternateReadingFrame();
            splitdnaSequence();
            assignAminos(i);
        }
    }
    
    /**
     * This method creates the library containing the names of the 20 essential amino acids and the code
     * for a stop codon.
     * @param aminoRep Decides which hashmap will be used.
     */
    private void createDNA_AminoDict(final String aminoRep) {
        switch (aminoRep) {
            case "one":
                dna_AminoDict.put("A", "GCT, GCC, GCA, GCG");
                dna_AminoDict.put("R", "CGT, CGC, CGA, CGG, AGA, AGG");
                dna_AminoDict.put("D", "GAT, GAC");
                dna_AminoDict.put("N", "AAT, AAC");
                dna_AminoDict.put("C", "TGT, TGC");
                dna_AminoDict.put("W", "CAA, CAG");
                dna_AminoDict.put("E", "GAA, GAG");
                dna_AminoDict.put("G", "GGT, GGC, GGA, GGG");
                dna_AminoDict.put("H", "CAT, CAC");
                dna_AminoDict.put("I", "ATT, ATC, ATA");
                dna_AminoDict.put("L", "TTA, TTG, CTT, CTC, CTA, CTG");
                dna_AminoDict.put("K", "AAA, AAG");
                dna_AminoDict.put("M", "ATG");
                dna_AminoDict.put("F", "TTT, TTC");
                dna_AminoDict.put("P", "CCT, CCC, CCA, CCG");
                dna_AminoDict.put("S", "TCT, TCC, TCA, TCG, AGT, AGC");
                dna_AminoDict.put("T", "ACT, ACC, ACA, ACG");
                dna_AminoDict.put("W", "TGG");
                dna_AminoDict.put("Y", "TAT, TAC");
                dna_AminoDict.put("V", "GTT, GTC, GTA, GTG");
                dna_AminoDict.put(" - STOP - ", "TAA, TGA, TAG");
                break;
            case "three":
                dna_AminoDict.put("Ala - ", "GCT, GCC, GCA, GCG");
                dna_AminoDict.put("Arg - ", "CGT, CGC, CGA, CGG, AGA, AGG");
                dna_AminoDict.put("Asp - ", "GAT, GAC");
                dna_AminoDict.put("Asn - ", "AAT, AAC");
                dna_AminoDict.put("Cys - ", "TGT, TGC");
                dna_AminoDict.put("Gln - ", "CAA, CAG");
                dna_AminoDict.put("Glu - ", "GAA, GAG");
                dna_AminoDict.put("Gly - ", "GGT, GGC, GGA, GGG");
                dna_AminoDict.put("His - ", "CAT, CAC");
                dna_AminoDict.put("Ile - ", "ATT, ATC, ATA");
                dna_AminoDict.put("Leu - ", "TTA, TTG, CTT, CTC, CTA, CTG");
                dna_AminoDict.put("Lys - ", "AAA, AAG");
                dna_AminoDict.put("Met - ", "ATG");
                dna_AminoDict.put("Phe - ", "TTT, TTC");
                dna_AminoDict.put("Pro - ", "CCT, CCC, CCA, CCG");
                dna_AminoDict.put("Ser - ", "TCT, TCC, TCA, TCG, AGT, AGC");
                dna_AminoDict.put("Thr - ", "ACT, ACC, ACA, ACG");
                dna_AminoDict.put("Trp - ", "TGG");
                dna_AminoDict.put("Tyr - ", "TAT, TAC");
                dna_AminoDict.put("Val - ", "GTT, GTC, GTA, GTG");
                dna_AminoDict.put("STOP -", "TAA, TGA, TAG");
                break;
            case "whole":
                dna_AminoDict.put("Alanine - ", "GCT, GCC, GCA, GCG");
                dna_AminoDict.put("Arginine - ", "CGT, CGC, CGA, CGG, AGA, AGG");
                dna_AminoDict.put("Aspartic Acid - ", "GAT, GAC");
                dna_AminoDict.put("Asparagine - ", "AAT, AAC");
                dna_AminoDict.put("Cysteïne - ", "TGT, TGC");
                dna_AminoDict.put("Glutamine - ", "CAA, CAG");
                dna_AminoDict.put("Glutamatic Acid - ", "GAA, GAG");
                dna_AminoDict.put("Glycine - ", "GGT, GGC, GGA, GGG");
                dna_AminoDict.put("Histidine - ", "CAT, CAC");
                dna_AminoDict.put("Isoleucine - ", "ATT, ATC, ATA");
                dna_AminoDict.put("Leucine - ", "TTA, TTG, CTT, CTC, CTA, CTG");
                dna_AminoDict.put("Lysine - ", "AAA, AAG");
                dna_AminoDict.put("Methionine - ", "ATG");
                dna_AminoDict.put("Phenylalanine - ", "TTT, TTC");
                dna_AminoDict.put("Proline - ", "CCT, CCC, CCA, CCG");
                dna_AminoDict.put("Serine - ", "TCT, TCC, TCA, TCG, AGT, AGC");
                dna_AminoDict.put("Threonine - ", "ACT, ACC, ACA, ACG");
                dna_AminoDict.put("Tryptophane - ", "TGG");
                dna_AminoDict.put("Tyrosine - ", "TAT, TAC");
                dna_AminoDict.put("Valine - ", "GTT, GTC, GTA, GTG");
                dna_AminoDict.put("STOP -", "TAA, TGA, TAG");
                break;
        }
    }
    
    /**
     * Method that obtains the substring from the uploaded string with the custom start/stop position. 
     */
    private void makeWorkSequence() {
        workSequence = dna_Sequence.substring(startPos, stopPos);
        switch (workSequence.length() % 3) {
            case 0:
                originalWorkSeq = workSequence;
                break;
            case 1:
                originalWorkSeq = workSequence.substring(0, workSequence.length() - 1);
                break;
            case 2:
                originalWorkSeq = workSequence.substring(0, workSequence.length() - 2);
                break;
            default:
                break;
        } 
    }
    
    /**
     * This method removes the first nucleotide from the original sequence and the complementary sequence
     * for the translation of another reading frame.
     */
    private void alternateReadingFrame() {
        workSequence = workSequence.substring(1);
        complementDNA = complementDNA.substring(1);
        
        if (alternateFrameCounter == 1) {
            switch (workSequence.length() % 3) {
            case 0:
                originalWorkSeq2 = workSequence;
                originalComplementWorkSeq2 = complementDNA;
                break;
            case 1:
                originalWorkSeq2 = workSequence.substring(0, workSequence.length() - 1);
                originalComplementWorkSeq2 = complementDNA.substring(0, complementDNA.length() - 1);
                break;
            case 2:
                originalWorkSeq2 = workSequence.substring(0, workSequence.length() - 2);
                originalComplementWorkSeq2 = complementDNA.substring(0, complementDNA.length() - 2);
                break;
            default:
                break;
        }
            alternateFrameCounter = 2;
        } else if (alternateFrameCounter == 2) {
            switch (workSequence.length() % 3) {
            case 0:
                originalWorkSeq3 = workSequence;
                originalComplementWorkSeq3 = complementDNA;
                break;
            case 1:
                originalWorkSeq3 = workSequence.substring(0, workSequence.length() - 1);
                originalComplementWorkSeq3 = complementDNA.substring(0, complementDNA.length() - 1);
                break;
            case 2:
                originalWorkSeq3 = workSequence.substring(0, workSequence.length() - 2);
                originalComplementWorkSeq3 = complementDNA.substring(0, complementDNA.length() - 2);
                break;
            default:
                break;
            }
        }
    }
    
    /**
     * This method makes the complementary string for the original DNA sequence. 
     */
    private void makeComplement() {
        StringBuilder complementSeqBuilder = new StringBuilder();
        for (int i = 0; i < workSequence.length(); i++) {
            char c = workSequence.charAt(i);
            switch (c) {
                case 'A': complementSeqBuilder.append("T");
                    break;
                case 'T': complementSeqBuilder.append("A");
                    break;
                case 'G': complementSeqBuilder.append("C");
                    break;
                case 'C': complementSeqBuilder.append("G");
                    break;
                default:
                    break;
            }
        }
        complementDNA = complementSeqBuilder.reverse().toString();
        switch (complementDNA.length() % 3) {
            case 0:
                originalComplementWorkSeq = complementDNA;
                break;
            case 1:
                originalComplementWorkSeq = complementDNA.substring(0, complementDNA.length() - 1);
                break;
            case 2:
                originalComplementWorkSeq = complementDNA.substring(0, complementDNA.length() - 2);
                break;
            default:
                break;
        }
    }
    
    /**
     * This method splits the original DNA strand and the complementary strand in triplets.
     */
    private void splitdnaSequence() {
        splitUpSeq = Arrays.toString(workSequence.split("(?<=\\G.{3})"));
        splitUpSeq = splitUpSeq.replaceAll("\\[", "");
        splitUpSeq = splitUpSeq.replaceAll("\\]", "");
        
        splitUpComp = Arrays.toString(complementDNA.split("(?<=\\G.{3})"));
        splitUpComp = splitUpComp.replaceAll("\\[", "");
        splitUpComp = splitUpComp.replaceAll("\\]", "");
    }
    
    /**
     * This method assigns amino acid names to the DNA triplets of both the original strand
     * and the complementary strand.
     */
    private void assignAminos() {
        StringBuilder originalAminos = new StringBuilder();
        for (String trip: splitUpSeq.split(", ")) {
            for (Map.Entry<String, String> entry : dna_AminoDict.entrySet()) {
                for (String code: entry.getValue().split(", ")) {
                    if (trip.matches(code)) {
                        originalAminos.append(entry.getKey());
                    }
                }
            }
        }
        originalAminoSeq = originalAminos.toString();
        
        StringBuilder complementAminos = new StringBuilder();
        for (String trip: splitUpComp.split(", ")) {
            for (Map.Entry<String, String> entry : dna_AminoDict.entrySet()) {
                for (String code: entry.getValue().split(", ")) {
                    if (trip.matches(code)) {
                        complementAminos.append(entry.getKey());
                    }
                }
            }
        }
        complementAminoSeq = complementAminos.toString();
    }
    
    /**
     * This method also assigns amino acids to the triplet DNA codes, but this method
     * is called upon after the reading frame has shifted. This method assigns the second and
     * third reading frame amino acid sequences to another variable with the help of the parameter that's
     * given along with the calling of this method.
     * @param i number that indicates to which string this amino acid sequence should be assigned. 
     */
    private void assignAminos(final int i) {
        StringBuilder originalAminos = new StringBuilder();
        for (String trip: splitUpSeq.split(", ")) {
            for (Map.Entry<String, String> entry : dna_AminoDict.entrySet()) {
                for (String code: entry.getValue().split(", ")) {
                    if (trip.matches(code)) {
                        originalAminos.append(entry.getKey());
                        break;
                    }
                }
            }
        }
        
        StringBuilder complementAminos = new StringBuilder();
        for (String trip: splitUpComp.split(", ")) {
            for (Map.Entry<String, String> entry : dna_AminoDict.entrySet()) {
                for (String code: entry.getValue().split(", ")) {
                    if (trip.matches(code)) {
                        complementAminos.append(entry.getKey());
                    }
                }
            }
        }
        if (i == 1) {
            originalAminoSeq1 = originalAminos.toString();
            complementAminoSeq1 = complementAminos.toString();
        } else if (i == 2) {
            originalAminoSeq2 = originalAminos.toString();
            complementAminoSeq2 = complementAminos.toString();
        }
        
    }
    
    /**
     * Method responsible for returning the uploaded string with the
     * custom start/stop positions to the servlet.
     * @return String with DNA sequence.
     */
    public final String originalWorkSeq() {
        return originalWorkSeq;
    }
    
    /**
     * Method responsible for returning the string containing the original used DNA
     * sequence with 1 frame shift.
     * @return String with DNA sequence featuring 1 frame shift. 
     */
    public final String originalWorkSeq2() {
        return originalWorkSeq2;
    }
    /**
     * Method responsible for returning the string containing the original used DNA
     * sequence with 2 frame shifts.
     * @return String with DNA sequence featuring 1 frame shift. 
     */
        public final String originalWorkSeq3() {
        return originalWorkSeq3;
    }
        
    /**
     * Method responsible for returning the amino acids of the forward strand, frame 1 to the servlet.
     * @return String containing amino acids sequence of forward frame 1.
     */
    public final String originalAminoAcids1() {
        return originalAminoSeq;
    }
    
    /**
     * Method responsible for returning the amino acids of the forward strand, frame 2 to the servlet.
     * @return String containing amino acids sequence of forward frame 2.
     */
    public final String originalAminoAcids2() {
        return originalAminoSeq1;
    }
    
    /**
     * Method responsible for returning the amino acids of the forward strand, frame 3 to the servlet.
     * @return String containing amino acids sequence of forward frame 3.
     */
    public final String originalAminoAcids3() {
        return originalAminoSeq2;
    }
    
    /**
     * Method responsible returning the complementary DNA sequence back to the servlet. 
     * @return String containing the complementary DNA sequence with custom start/stop positions.
     */
    public final String originalComplementWorkSeq() {
        return originalComplementWorkSeq;
    }
    
    /**
     * Method responsible for returning the complementary DNA sequence with 1 frame shift.
     * @return String containing the DNA string of the used complementary DNA string with 1
     * frame shift.
     */
    public final String originalComplementWorkSeq2() {
        return originalComplementWorkSeq2;
    }
    
    /**
     * Method responsible for returning the complementary DNA sequence with 2 frame shifts.
     * @return String containing the DNA string of the used complementary DNA string with 2
     * frame shifts.
     */
    public final String originalComplementWorkSeq3() {
        return originalComplementWorkSeq3;
    }
    
    /**
     * Method responsible for returning the amino acids sequence of the reverse strand, frame 1 to the servlet.
     * @return String containing amino acid sequence of reverse frame 1.
     */
    public final String complementaryAminoAcids1() {
        return complementAminoSeq;
    }
    
    /**
     * Method responsible for returning the amino acids sequence of the reverse strand, frame 2 to the servlet.
     * @return String containing amino acid sequence of reverse frame 2.
     */
    public final String complementaryAminoAcids2() {
        return complementAminoSeq1;
    }
    
    /**
     * Method responsible for returning the amino acids sequence of the reverse strand, frame 3 to the servlet.
     * @return String containing amino acid sequence of reverse frame 3.
     */
    public final String complementaryAminoAcids3() {
        return complementAminoSeq2;
    }
}
