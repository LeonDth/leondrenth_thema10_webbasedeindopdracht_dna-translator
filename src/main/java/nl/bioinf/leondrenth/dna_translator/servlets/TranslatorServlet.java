/*
 * Copyright (c) 2016 Leon Drenth [leon-drenth@hotmail.com].
 * All rights reserved
 * Version 0.0.1
 */
package nl.bioinf.leondrenth.dna_translator.servlets;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import nl.bioinf.leondrenth.dna_translator.model.dnaChecker;
import nl.bioinf.leondrenth.dna_translator.model.dnaTranslator;

/**
 * Servlet responsible for managing the back-end of the web page. 
 * @author Leon
 */
@WebServlet(name="DNATranslateServlet", urlPatterns = {"/Translate.do"})
@MultipartConfig
public class TranslatorServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
//        try (PrintWriter out = response.getWriter()) {
//            /* TODO output your page here. You may use following sample code. */
//            out.println("<!DOCTYPE html>");
//            out.println("<html>");
//            out.println("<head>");
//            out.println("<title>Servlet TranslatorServlet</title>");            
//            out.println("</head>");
//            out.println("<body>");
//            out.println("<h1>Servlet TranslatorServlet at " + request.getContextPath() + "</h1>");
//            out.println("</body>");
//            out.println("</html>");
//        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        HttpSession session = request.getSession();
        
        String userSequence = request.getParameter("DNA_Seq").toUpperCase().replaceAll("\\s+", "");
        int startPos = 0;
        int stopPos = userSequence.length() - 1;
        
        if (!"".equals(request.getParameter("start_pos"))) { 
            startPos = Integer.parseInt(request.getParameter("start_pos")) - 1;
        } 
        if (!"".equals(request.getParameter("stop_pos"))) {
            stopPos = Integer.parseInt(request.getParameter("stop_pos")) - 1;
        }
            
        request.setAttribute("Raw_DNA_String", userSequence);
        String viewJSP = "TranslatedDNA.jsp";
        
        dnaChecker checker = new dnaChecker(userSequence);
        if (checker.errorPresence()) {
            viewJSP = "index.jsp";
            request.setAttribute("Error_number", 1);
        } else if (
               startPos >= userSequence.length() - 1
            || stopPos >= userSequence.length()
            || stopPos <= startPos 
            || startPos <= -1 
            || stopPos <= -1
            || stopPos - startPos < 3) {
                viewJSP = "index.jsp";
                request.setAttribute("Error_number", 2);
        } else {
            String aminoRep = request.getParameter("mySelect");
            dnaTranslator translator = new dnaTranslator(userSequence, startPos, stopPos);
            translator.initializer(aminoRep);
            
            session.setAttribute("OriginalWorkSeq", translator.originalWorkSeq());
            session.setAttribute("OriginalWorkSeq2", translator.originalWorkSeq2());
            session.setAttribute("OriginalWorkSeq3", translator.originalWorkSeq3());
            session.setAttribute("OriginalAminoAcids1", translator.originalAminoAcids1());
            session.setAttribute("OriginalAminoAcids2", translator.originalAminoAcids2());
            session.setAttribute("OriginalAminoAcids3", translator.originalAminoAcids3());
            
            session.setAttribute("OriginalComplementWorkSeq", translator.originalComplementWorkSeq());
            session.setAttribute("OriginalComplementWorkSeq2", translator.originalComplementWorkSeq2());
            session.setAttribute("OriginalComplementWorkSeq3", translator.originalComplementWorkSeq3());
            session.setAttribute("ComplementAminoAcids1", translator.complementaryAminoAcids1());
            session.setAttribute("ComplementAminoAcids2", translator.complementaryAminoAcids2());
            session.setAttribute("ComplementAminoAcids3", translator.complementaryAminoAcids3());
        }
        
        RequestDispatcher view = request.getRequestDispatcher(viewJSP);
        view.forward(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
