# DNA translating Web-Interface Repo #

Welcome to the repository containing the NetBeansProject for the web-interface of my DNA translator. The website takes a DNA sequence input and gives as output the essential amino acid sequences of all 6 reading frames with stop codons. By doing this, the time-consuming step of the transcription process can be skipped. The web-interface also allows a custom start and stop position for the sequence to be translated. Be aware that this web-interface does NOT detect exons and introns! This project was done as the final assignment for the WebBassedInfSystems1 course at the Hanzehogeschool ILST. 

### What does this repository contain? ###

* Clonable NetBeansProject containing the source code for the interface (Version 1.0)
* Java documentation for source code

See the source tab for all of these files

### How do I get set up? ###

* This repository can be cloned into the NetBeans IDE. After cloning the project, it can be run by Netbeans. This results in the web-interface being shown by the primary internet browser. Make sure to clean and build the project if an older version of this web interface was already cloned to NetBeans earlier. After running the project, see the first web page for further instructions.

* The interface needs a nucleotide sequence to translate. To obtain a randomly generated DNA sequence, the following website could be of use: http://www.faculty.ucr.edu/~mmaduro/random.htm

### Changes in the newest version of this web interface ###

- Added javascript to the web interface. The index web page now shows the number of characters in the DNA sequence. The result page hides all the DNA reading frames. Clicking a button shows the DNA sequence, making the screen less cluttered when long sequences are being shown.
- Added sessions, allowing the user to switch between the input and the output page without the results disappearing from the output page. The results can be reaccessed by going back to the output page if the user didn't close the web page or if the user didn't put in a new DNA sequence. 
- Added an option bar that allows for the user to select in what way the amino acid sequences will be shown. Currently allows 3 different ways of representing amino acids: single characters, 3 characters or whole names.
- Improved variable and class names of the back-end to satisfy the current java naming conventions.
- More input sequence improvisation. Nucleotides can now be put in lower case letters and the back-end ignores any newlines and spaces in the sequence.
- Different wording of front-end text for more clarity.

### Made by: ###

* Leon Drenth [l.r.drenth@st.hanze.nl], Hanzehogeschool Groningen, ILST, Third-year Bioinformatics student